package org.bitbucket.nobby451.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class FooterSurfaceView extends SurfaceView implements SurfaceHolder.Callback, Runnable {

    private Thread thread;
    private int touchedIndex = -1;

    public FooterSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        getHolder().setFormat(PixelFormat.TRANSLUCENT);
        setZOrderOnTop(true);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        (thread = new Thread(this)).start();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        thread = null;
    }

    public void run() {
        Canvas canvas;
        int[] upDrawableIds = {R.drawable.footer1_up, R.drawable.footer2_up, R.drawable.footer3_up, R.drawable.footer4_up, R.drawable.footer5_up};
        int[] downDrawableIds = {R.drawable.footer1_down, R.drawable.footer2_down, R.drawable.footer3_down, R.drawable.footer4_down, R.drawable.footer5_down};
        BitmapDrawable[] upDrawables = new BitmapDrawable[upDrawableIds.length];
        BitmapDrawable[] downDrawables = new BitmapDrawable[downDrawableIds.length];
        int offset = 0;
        for (int i = 0; i < upDrawables.length; i++) {
            upDrawables[i] = (BitmapDrawable) getResources().getDrawable(upDrawableIds[i]);
            upDrawables[i].setBounds(offset, 0, upDrawables[i].getIntrinsicWidth() + offset, upDrawables[i].getIntrinsicHeight());
            downDrawables[i] = (BitmapDrawable) getResources().getDrawable(downDrawableIds[i]);
            downDrawables[i].setBounds(offset, 0, downDrawables[i].getIntrinsicWidth() + offset, downDrawables[i].getIntrinsicHeight());
            offset += upDrawables[i].getIntrinsicWidth();
        }
        int counter = 0;
        while (thread != null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            canvas = getHolder().lockCanvas();
            if (canvas == null) {
                continue;
            }
            float scale = (float) getWidth() / offset;
            canvas.scale(scale, scale);
            for (int i = 0; i < upDrawables.length; i++) {
                if (i == touchedIndex) {
                    downDrawables[i].draw(canvas);
                } else {
                    upDrawables[i].draw(canvas);
                }
            }
            getHolder().unlockCanvasAndPost(canvas);
            touchedIndex = ((++counter) % 60 / 10) - 1;
        }
    }

}
