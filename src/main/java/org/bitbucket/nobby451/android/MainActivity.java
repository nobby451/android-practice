package org.bitbucket.nobby451.android;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

public class MainActivity extends Activity {

    /**
     * Called when the activity is first created.
     * @param savedInstanceState If the activity is being re-initialized after 
     * previously being shut down then this Bundle contains the data it most 
     * recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
     */
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DisplayMetrics outMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(outMetrics);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) findViewById(R.id.relativeLayout).getLayoutParams();
        int widthPixels = outMetrics.widthPixels;
        int heightPixels = outMetrics.heightPixels;
        if (widthPixels / 2 < heightPixels / 3) {
            layoutParams.topMargin = layoutParams.bottomMargin = (heightPixels - widthPixels * 3 / 2) / 2;
        } else {
            layoutParams.leftMargin = layoutParams.rightMargin = (widthPixels - heightPixels * 2 / 3) / 2;
        }
        WebView webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new JSObject(), "android");
        webView.loadUrl("http://10.0.2.2:10080/index.html");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    class JSObject {
        public String readAsset() {
            try {
                InputStream inputStream = getResources().getAssets().open("data.txt");
                byte[] buffer = new byte[256];
                int len;
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while ((len = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, len);
                }
                return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.NO_WRAP);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
